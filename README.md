# arabic2english

I created this in a very modular way. All numerical configuration are liberally
configurable in number.yml.

```
$ ruby arabic2.english.rb 1
one
$ ruby arabic2.english.rb 11
eleven
$ ruby arabic2.english.rb 111
one hundred and eleven
$ ruby arabic2english.rb 9999
nine thousand and nine hundred and ninety nine
```

It supports Japanese too! :)

```
$ ruby arabic2japanese.rb 223
ni-hyaku ni-ju san
$ ruby arabic2japanese.rb 151
hyaku go-ju ichi
$ ruby arabic2japanese.rb 302
san-byaku ni 
$ ruby arabic2japanese.rb 469
yon-hyaku roku-ju kyu
$ ruby arabic2japanese.rb 2025
ni-sen ni-ju go
$ ruby arabic2japanese.rb 202582493284023482
$ ni-ju-kei ni-sen go-hyaku hachi-ju ni-cho yon-sen kyu-hyaku san-ju ni-oku hachi-sen yon-hyaku ni-man san-sen yon-hyaku hachi-ju ni
```

The Japanese numbering version may be imperfect, I try to learn Japanese numerals
while doing this (I hope it performs correctly). With the Japanese version just
a YAML definition, it demonstrates how flexible is the underlying system. I am confident I can
implement the Bahasa Indonesia version in 10 minutes.

## Tests

Tests are written for RSpec.

## References

- [Japanese numerals](https://en.wikipedia.org/wiki/Japanese_numerals).
- [Spanish numerals](http://www.donquijote.org/spanishlanguage/spanish-numbers/).
- [Spanish numerals (other)](https://www.thoughtco.com/counting-the-cardinal-numbers-3078118).
